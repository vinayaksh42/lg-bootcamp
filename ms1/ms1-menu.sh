#===============================================================================
#USAGE : ./ms1-menu.sh orientation
#DESCRIPTION : Opens a terminal menu to execute restart, shutdown, turns-creen, keyboard-language scripts.
#PARAMS: 
#TIPS:
#===============================================================================
#
#!/bin/bash
#START
PS3="Enter the operation: "
select opt in Shutdown Restart Turn-Screen "Change keyboard language";
do
  case $opt in
        "Shutdown")
            sh ./shutdown.sh
            ;;
        "Restart")
            sh ./restart.sh
            ;;
        "Turn-Screen")
            echo Input the direction in which you want to turn the screen:
            read direction
            sh ./turn-screen.sh $direction
            ;;
        "Change keyboard language")    
            echo What is the language code that you want?
            read lang
            sh ./keyboard-language.sh $lang    
            ;;
  esac
done
#END
#
# Copyright Liquid Galaxy ORG.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
